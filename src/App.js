import './App.css';
import RouteIndex from "./app/routing/RouteIndex";

function App() {
    return (
        <>
            <RouteIndex/>
        </>
    )
}

export default App;
