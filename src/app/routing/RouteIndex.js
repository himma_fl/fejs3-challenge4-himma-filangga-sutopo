import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import MasterLayout from "../layout/MasterLayout";
import LandingPage from "../pages/home-page/LandingPage";
import ResultPage from "../pages/search-result/ResultPage";
import DetailPage from "../pages/detail-page/DetailPage";
import PaymentPage from "../pages/payment-page/PaymentPage";

const RouteIndex = () => {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<MasterLayout content={<LandingPage/>} />}/>
                    <Route path="/search" element={<MasterLayout content={<ResultPage/>} />}/>
                    <Route path="/detail/:id" element={<MasterLayout content={<DetailPage/>} />}/>
                    <Route path="/payment" element={<MasterLayout content={<PaymentPage/>} />}/>
                    <Route path="*" element={<h1>404 Not Found</h1>}/>
                </Routes>
            </BrowserRouter>
        </>
    )
}

export default RouteIndex