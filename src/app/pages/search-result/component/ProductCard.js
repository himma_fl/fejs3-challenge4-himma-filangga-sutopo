import {Row, Col, Card, Typography, Space, Button, Spin} from 'antd';
import {Product_Car, Calendar, User, Setting} from '../../../../assets/media'
import {useEffect, useState} from "react";
import axios from "axios";
import {useNavigate} from "react-router-dom";

const {Text, Link} = Typography;

const ProductCard = () => {
    let navigate = useNavigate();
    const [dataList, setDataList] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getListCar()
    }, []);

    const getListCar = async () => {
        try {
            setLoading(true)
            let result = await axios.get(`https://rent-cars-api.herokuapp.com/admin/car`)
            let data = result.data
            setDataList(data)
        } catch (err) {
            console.log(err)
        } finally {
            setLoading(false)
        }
    }

    const handleSubmit = (event) => {
        let id = event.currentTarget.value
        navigate(`/detail/${id}`);
    }

    return (
        <>
            {loading ? <Spin className="d-flex justify-content-center align-items-center"/> : ""}
            <Row gutter={[24, 24]}>
                {dataList?.map((item) => {
                    return (
                        <Col classame="gutter-row" span={8}>
                            <Card bordered={false}
                                  style={{
                                      width: "100%",
                                      boxShadow: "0px 0px 4px rgba(0, 0, 0, 0.15)",
                                      borderRadius: 8,
                                  }}>
                                <div className="text-center mt-4 mb-5">
                                    <img src={item.image} alt="" style={{objectFit: "cover", width: 270, height: 160}}/>
                                </div>
                                <Space direction="vertical" size={16} style={{display: 'flex'}}>
                                    <Space direction="vertical" size={8} style={{display: 'flex'}}>
                                        <Text>{item.name}</Text>
                                        <Text className="fw-bold fs-6">{item.price.toLocaleString('id-ID', {
                                            style: 'currency',
                                            currency: 'IDR',
                                            minimumFractionDigits: 0,
                                            maximumFractionDigits: 0,
                                        })} / hari</Text>
                                        <Text className="fw-light">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit, sed
                                            do
                                            eiusmod tempor
                                            incididunt ut labore et dolore magna aliqua. </Text>
                                    </Space>

                                    <Space size={8} style={{display: 'flex'}}>
                                        <img src={User} alt=""/>
                                        <Text>4 Orang</Text>
                                    </Space>
                                    <Space size={8} style={{display: 'flex'}}>
                                        <img src={Setting} alt=""/>
                                        <Text>Manual</Text>
                                    </Space>
                                    <Space size={8} style={{display: 'flex'}}>
                                        <img src={Calendar} alt=""/>
                                        <Text>Tahun 2020</Text>
                                    </Space>
                                </Space>
                                <Button
                                    className="mt-4"
                                    style={{
                                        background: "#5CB85F",
                                        borderRadius: 2,
                                        color: "white",
                                        fontWeight: 700,
                                        width: "100%",
                                        height: 48
                                    }}
                                    value={item.id}
                                    onClick={handleSubmit}
                                >
                                    Pilih Mobil
                                </Button>
                            </Card>
                        </Col>
                    )
                })}
            </Row>
        </>
    )
}

export default ProductCard