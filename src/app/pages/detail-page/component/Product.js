import {Button, Card, message, Space, Typography} from "antd";
import {Calendar, Product_Car, Setting, User} from "../../../../assets/media";
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";

const {Text, Link} = Typography;

const Product = () => {
    const [dataList, setDataList] = useState([])
    let {id} = useParams();

    useEffect(() => {
        getCarById(id)
    }, []);

    const getCarById = async (id) => {
        try {
            let result = await axios.get(`https://rent-cars-api.herokuapp.com/admin/car/${id}`)
            let data = result.data
            setDataList(data)
            console.log(data)
        } catch (err) {
            console.log(err)
        }
    }

    const error = () => {
        message.error('Halaman belum ada');
    };

    return (
        <>
            <Card bordered={false}
                  style={{
                      width: "100%",
                      boxShadow: "0px 0px 4px rgba(0, 0, 0, 0.15)",
                      borderRadius: 8
                  }}>
                <div className="text-center mt-4 mb-5">
                    <img src={Product_Car} alt=""/>
                </div>
                <Space direction="vertical" size={48} style={{display: 'flex'}}>
                    <Space direction="vertical" size={9} style={{display: 'flex'}}>
                        <Space direction="vertical" size={8} style={{display: 'flex'}}>
                            <Text className="fw-bold">{dataList.name}</Text>
                        </Space>
                        <Space size={16} style={{display: 'flex'}}>
                            <Space size={8} style={{display: 'flex'}}>
                                <img style={{width: 12}} src={User} alt=""/>
                                <Text type="secondary" className="product-category">4 Orang</Text>
                            </Space>
                            <Space size={8} style={{display: 'flex'}}>
                                <img style={{width: 12}} src={Setting} alt=""/>
                                <Text type="secondary" className="product-category">Manual</Text>
                            </Space>
                            <Space size={8} style={{display: 'flex'}}>
                                <img style={{width: 12}} src={Calendar} alt=""/>
                                <Text type="secondary" className="product-category">Tahun 2020</Text>
                            </Space>
                        </Space>
                    </Space>
                    <div>
                        <div className="d-inline"><Text>Total</Text></div>
                        <div className="d-inline" style={{float: "right"}}><Text className="fw-bold">
                            {dataList.price?.toLocaleString('id-ID', {
                                style: 'currency',
                                currency: 'IDR',
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 0,
                            })}
                        </Text>
                        </div>
                    </div>
                </Space>
                <Button
                    className="mt-4"
                    style={{
                        background: "#5CB85F",
                        borderRadius: 2,
                        color: "white",
                        fontWeight: 700,
                        width: "100%",
                        height: 36
                    }}
                    onClick={error}
                >
                    Lanjutkan Pembayaran
                </Button>
            </Card>
        </>
    )
}

export default Product